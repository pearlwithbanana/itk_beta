
            <div class="pag-nav">
                <ul class="p-list">
                    <li><a href='<?= site_url('home') ?>'>Powrót na stronę główną</a></li> &nbsp;&nbsp;/&nbsp;
                    <li><a href='<?= site_url('login') ?>'>Zaloguj</a></li> &nbsp;&nbsp;&nbsp;
                </ul>
            </div>
            <div class="coats">
                <h3 class="c-head">Podaj dane, aby zresetować swoje hasło.</h3>
                <p>...i znów cieszyć się z możliwości korzystania z serwisu :)</p>
            </div>
            <div class="register">
                <?php
                if (validation_errors()) {
                    echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
                }
                ?>
                <div class="register-but">
                    <form method="post">

                        <div class="register-bottom-grid">
                            <h3>Wypełnij poniższe pola.</h3>
                            <div>
                                <span>Podaj nowe hasło.</span>
                                <input id = 'password' name = 'password' value = "<?php echo set_value('mail'); ?>" type = "password">
                            </div>
                            <div>
                                <span>Potwierdź nowe hasło.</span>
                                <input id = 'passconf' name = 'passconf' value = "<?php echo set_value('passconf'); ?>" type = "password">
                            </div>
                            <div class = "clearfix"></div>
                            <div id = "emailconfirm"></br></div>


                            <BR></BR>



                            <div class="register-but">
                                <input type="submit" value="Zresetuj hasło!"/>
                            </div>
                        </div>

                    </form>
                    <div class="clearfix"> </div> 
                </div>


            </div>
