

<!-- Banner Starts Here --->

<div class="banner">
    <div id ="roundbanner"> <!--- początek okrągłego diva --->

        <h1>Kup, wypożycz, oceń!</h1>
        <small>Książka i możliwość czytania, to jeden z największych cudów ludzkiej cywilizacji.
        </small>
        <div class="ban-btn">
            <a href="#">Przejdź do sklepu</a>
        </div>

    </div> <!--- koniec okrągłego diva --->
</div>

<!-- Banner Ends Here --->
<!-- Gallery Starts Here --->
<div class="gallery">
    <div class="gallery-row">
        <div class="gallery-grid">
            <?php $this->Book_model->most_favoriet_books(0); ?>
        </div>	
    </div>
    <div class="gallery-row">
        <div class="gallery-grid">
            <?php $this->Book_model->most_favoriet_books(2); ?>
        </div>	
    </div>
    <div class="gallery-row">
        <div class="gallery-grid">
            <?php $this->Book_model->most_favoriet_books(4); ?>
        </div>	
    </div>
    <div class="gallery-row">
        <div class="gallery-grid">
            <?php $this->Book_model->most_favoriet_books(6); ?>
        </div>	
    </div>

</div>

<!-- Gallery Ends Here --->


