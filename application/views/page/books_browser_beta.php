<!-- nie wiem co to<script>
    $(document).ready(function () {
        $('#table').dataTable();
    });</script>-->
<div id="books_browser">
    <form class="form-inline" method="POST">
        <div id="books_select">
            <div class="books_browser_text">
                <input type="text" class="form-control form-control-custom" id="books_input" placeholder="Wpisz interesującą Cię frazę"/></br>
            </div>
            <div class="books_browser_paragraph" >
                <p>...lub wybierz swoje kryteria</p>
            </div>
            <select class="form-control" name="author" id="author_id">
                <option selected="selected" class="books_browser_option">Autor</option>
                <option>El James</option>
                <option>JK Rowing</option>
                <option>Agatha Christie</option>
            </select> 
            <select class="form-control" name="tittle"> 
                <option selected="selected" class="books_browser_option">Tytuł</option>
                <option>50 Twarzy Greya</option>
                <option>Harry Potter</option>
                <option>Morderstwo w Oreint Expressie</option>
            </select> 
            <select class="form-control" name="minPrice">
                <option selected="selected" class="books_browser_option">Cena od</option>
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>60</option>
                <option>75</option>
                <option>90</option>
            </select> 
            <select class="form-control" name="maxPrice">
                <option selected="selected" class="books_browser_option">Cena do</option>
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>60</option>
                <option>75</option>
                <option>90</option>
            </select>
            <select class="form-control" name="minRate">
                <option selected="selected" class="books_browser_option">Ocena od</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select> 
            <select class="form-control" name="maxRate">
                <option selected="selected" class="books_browser_option">Ocena do</option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select> 
            <select class="form-control" name="condition">
                <option selected="selected" class="books_browser_option">Stan</option>                
                <option>Nowa</option>
                <option>Używana</option>
            </select>
            <select class="form-control" name="order">
                <option selected="selected" class="books_browser_option">Sortuj wg</option>                
                <option>Cena : rosnąco</option>
                <option>Cena : malejąco</option>
                <option>Ocena : malejąco</option>
                <option>Ocena : rosnąco</option>
            </select>
            <div id="fire" class="btn btn-default books_browser_button">Wyszukaj!</div>

    </form>
</div>
</div>
<div align="center">
    <ul class="pagination">
        <li><a href="">A</a></li>
        <li><a href="">B</a></li>
        <li><a href="">C</a></li>
        <li><a href="">D</a></li>
        <li><a href="">E</a></li>
        <li><a href="">F</a></li>
        <li><a href="">G</a></li>
        <li><a href="">H</a></li>
        <li><a href="">I</a></li>
        <li><a href="">J</a></li>
        <li><a href="">K</a></li>
        <li><a href="">L</a></li>
        <li><a href="">M</a></li>
        <li><a href="">N</a></li>
        <li><a href="">O</a></li>
        <li><a href="">P</a></li>
        <li><a href="">R</a></li>
        <li><a href="">S</a></li>
        <li><a href="">T</a></li>
        <li><a href="">U</a></li>
        <li><a href="">W</a></li>
        <li><a href="">X</a></li>
        <li><a href="">Y</a></li>
        <li><a href="">Z</a></li>
    </ul>
</div>

<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">

<!--<thead>
    <tr>
        <th width="50">Ocena</th>
        <th width="20">Okładka</th>
        <th width="150">Tytuł</th>
        <th width="100">Autor</th>
        <th width="50">Stan</th>
        <th width="50">Cena</th>
        <th width="80">Sprzedający</th>
    </tr>
</thead>-->

    <tbody>


    <div id="user_mixed_data">
        <?php
        /*      foreach ($results as $row) {
          $condition = $row->condition;
          if ($condition == 0) {
          $condition = 'Używana';
          } else {
          $condition = 'Nowa';
          }
          echo "<tr>";
          echo "<td><h3>" . $row->grade . "</h3></td>";
          echo "<td><img width='50'heigth='75' src='" . $row->image_path . "'/></td>";
          echo "<td>" . $row->tittle . "</td>";
          echo "<td>" . $row->author . "</td>";
          echo "<td>" . $condition . "</td>";
          echo "<td>" . $row->price . " ,-</td>";
          echo "<td>" . $row->seller . "</td>";
          echo "</tr>";
          } */
        ?>
    </div>

</tbody>
</table>
<ul class="pagination">

    <?php echo $links; ?>

</ul>
<!--Mój AJAX-->
<div id="test"></div>
<script>

    //mouseenter
    $('#fire').click(function () {
        data = {
            author: $('[name=author]').val(),
            tittle: $('[name=tittle]').val(),
            minPrice: $('[name=minPrice]').val(),
            maxPrice: $('[name=maxPrice]').val(),
            minRate: $('[name=minRate]').val(),
            maxRate: $('[name=maxRate]').val(),
            condition: $('[name=condition]').val(),
            order: $('[name=order]').val()           
        }
        page = parseInt($('#current_page').text());
        $.ajax(
                {
                    type: 'POST',
                    url: 'Baza_ksiazek/index/page',
                    data: data,
                    success: function (feedback)
                    {
                        $('tbody').html(feedback);
                        console.log(feedback);
                    }
                })
    })


</script>


