</br>

<?php
$book_info = $this->Book_model->get_book_info($link);
?>
<div class="coats">
    <h3 class="c-head"><?= $book_info['tittle'] ?></h3>
    <p><?= $book_info['author'] ?></p>
</div>
<div class="book_view">
    <div class="book_image">
        <img src='<?= $book_info['image_path'] ?>'/>
    </div>
    <div class="book_description">

        <div class = 'rating_star'>
            <?php
            $round_grade = round($book_info['grade']);
            
            
            for ($i = 1; $i < $round_grade + 1; $i++) {
                echo " <div class='yellow_star'><div class = 'rate_book' id='book_$i'><span class='glyphicon glyphicon-star'></span></div></div>  ";
            }
            if ($round_grade == 0) {
                $round_grade = 1;
            }
            
            for ($i = $round_grade + 1; $i < 11; $i++) {
                echo " <div class = 'black_star'><div class = 'rate_book' id='book_$i'><span class='glyphicon glyphicon-star'></span></div></div>  ";
            }
            ?>

            <div class='book_rate'>
                <h1 id='book_rate'>
                    <?= $book_info['grade'] ?>/10
                </h1>
            </div>
        </div>

        </br>
        <p>Sprzedający: <?= $book_info['seller'] ?></p>
        <h2><?= $book_info['description'] ?></h2>

    </div>
    <script>
        $('.rate_book').click(function ()
        {
            number = <?= $this->uri->segment(3); ?>;
            rate = $(this).attr('id');
            rate = rate.replace('book_', '');
            console.log('handlers/rate_book/book/' + number + '/' + rate);
            $.ajax(
                    {
                        url: 'handlers/rate_book/book/' + number + '/' + rate,
                        success: function(data)
                        {
                            $('#book_rate').html(data+'/10')
                        }
                    })
        }
        )
    </script>
</div>
</br>



<?php $this->Comment_model->get_comment_info($link); ?>

<?php
if ($this->session->userdata('is_logged')) {

    echo
    "<form method='post' action='handlers/add_comment/ksiazka/$link'>"
    . '<label>Dodaj komentarz</label>'
    . '<textarea class="form-control" rows="3" name="comment" value=""></textarea></br>'
    . '<input type=submit  name = "submit_button" class = "btn btn-default" value="Skomentuj"/>'
    . '</form>';
} else {
    echo "<div class='coats'>Musisz się zalogować, aby móc dodawać komentarze. <a href='Zaloguj'>Kliknij tutaj</a> aby się zalogować</div>";
}
?>

<div>



</div>

</br></br></br>
