<!-- nie wiem co to<script>
    $(document).ready(function () {
        $('#table').dataTable();
    });</script>-->
<div id="whole_page">
    <div id="books_browser">
        <form class="form-inline" method="POST" action="Baza_ksiazek/strona">
            <div id="books_select">
                <div class="books_browser_text">
                    <input type="text" class="form-control form-control-custom" id="books_input" placeholder="Wpisz interesującą Cię frazę"/></br>
                </div>
                <div class="books_browser_paragraph" >
                    <p>...lub wybierz swoje kryteria</p>
                </div>
                <?php
                $fields = $this->session->userdata('fields');
                ?>
                <select class="form-control" name="author" id="author_id">
                    <option selected="selected" class="books_browser_option"><?php
                if ($fields) {
                    echo $fields['author'];
                } else {
                    echo "Autor";
                }
                ?></option>
                    <option>El James</option>
                    <option>JK Rowing</option>
                    <option>Agatha Christie</option>
                </select> 
                <select class="form-control" name="tittle"> 
                    <option selected="selected" class="books_browser_option"><?php
                        if ($fields) {
                            echo $fields['tittle'];
                        } else {
                            echo "Tytuł";
                        }
                ?></option>

                    <div id="tittle_options">

                    </div>

            </div>
            </select> 
            <select class="form-control" name="minPrice">
                <option selected="selected" class="books_browser_option"><?php
                        if ($fields) {
                            if ($fields['minPrice'] == 0) {
                                echo "Cena od";
                            } else {
                                echo $fields['minPrice'];
                            }
                        } else {
                            echo "Cena od";
                        }
                ?></option>
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>60</option>
                <option>75</option>
                <option>90</option>
            </select> 
            <select class="form-control" name="maxPrice">
                <option selected="selected" class="books_browser_option"><?php
                    if ($fields) {

                        if ($fields['maxPrice'] == 0) {
                            echo "Cena do";
                        } else {
                            echo $fields['maxPrice'];
                        }
                    } else {
                        echo "Cena do";
                    }
                ?></option>
                <option>5</option>
                <option>10</option>
                <option>15</option>
                <option>25</option>
                <option>35</option>
                <option>45</option>
                <option>60</option>
                <option>75</option>
                <option>90</option>
            </select>
            <select class="form-control" name="minRate">
                <option selected="selected" class="books_browser_option"><?php
                    if ($fields) {
                        if ($fields['minRate'] == 0) {
                            echo "Ocena od";
                        } else {
                            echo $fields['minRate'];
                        }
                    } else {
                        echo "Ocena od";
                    }
                    ?></option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select> 
            <select class="form-control" name="maxRate">
                <option selected="selected" class="books_browser_option"><?php
                    if ($fields) {
                        if ($fields['maxRate'] == 0) {
                            echo "Ocena do";
                        } else {
                            echo $fields['maxRate'];
                        }
                    } else {
                        echo "Ocena do";
                    }
                    ?></option>
                <option>1</option>
                <option>2</option>
                <option>3</option>
                <option>4</option>
                <option>5</option>
                <option>6</option>
                <option>7</option>
                <option>8</option>
                <option>9</option>
                <option>10</option>
            </select> 
            <select class="form-control" name="condition">
                <option selected="selected" class="books_browser_option"><?php
                    if ($fields) {
                        echo $fields['condition'];
                    } else {
                        echo "Stan";
                    }
                    ?></option>                
                <option>Nowa</option>
                <option>Używana</option>
            </select>
            <select class="form-control" name="order">
                <option selected="selected" class="books_browser_option"><?php
                    if ($fields) {
                        echo $fields['order'];
                    } else {
                        echo "Sortuj według";
                    }
                    ?></option>                
                <option>Cena: rosnąco</option>
                <option>Cena: malejąco</option>
                <option>Ocena: malejąco</option>
                <option>Ocena: rosnąco</option>
            </select>
            <input type='submit' class="btn btn-default books_browser_button" value="Wyszukaj!"/>            
            <div id="clear" class="btn btn-default books_browser_button">Wyczyść!</div>

        </form>
    </div>
</div>


<table id="table" class="table table-striped table-bordered" cellspacing="0" width="100%">
    <div id="user_mixed_data">
<?php
if (is_array($result)) {
    foreach ($result as $row) {
        echo $row;
    }
} else {
    echo $result;
}
?>
    </div>
</table>
<ul class="pagination">

<?php
echo $links;
?>

</ul>
<!--Mój AJAX-->
<div id="test"></div>
</div>
<script>

    //mouseenter
    $('#clear').click(function () {
        $.ajax(
                {
                    type: 'POST',
                    url: 'handlers/clear_form',
                    success: function (feedback)
                    {
                        $('#whole_page').html(feedback);

                    }
                })
    })
    $('[name=author]').change(function () {
        author = $('[name=author]').val();
        $.ajax(
                {
                    type: 'POST',
                    url: 'handlers/get_form_data/tittle/' + author,
                    success: function (feedback)
                    {
                        $('[name=tittle]').html(feedback);
                        console.log(feedback);
                    }
                }
        )
    })
    /*$('[name=tittle]').blur(function () {
        author = $('[name=author]').val();
        $.ajax(
                {
                    type: 'POST',
                    url: 'handlers/get_form_data/tittle/' + author,
                    success: function (feedback)
                    {
                        $('[name=tittle]').html(feedback);
                        console.log(feedback);
                    }
                }
        )
    })
    */
    $('Buy_now').mouseenter(function () {
        $.ajax(
                {
                    type: POST,
                    url: 'handlers/is_able_to_buy'
                }
        )
    })

</script>


