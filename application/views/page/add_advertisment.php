<?php $this->load->helper(array('captcha', 'string')); ?>

<div class="pag-nav">
    <ul class="p-list">
        <li><a href='Start'>Powrót na stronę główną</a></li> &nbsp;&nbsp;/&nbsp;
        <li><a href='Zaloguj'>Zaloguj</a></li> &nbsp;&nbsp;&nbsp;
    </ul>
</div>
<div class="coats">
    <h3 class="c-head">Dodaj ogłoszenie</h3>
    <p>...i nie zapomnij podzielić się opinią na temat przeczytanej książki!</p>
</div>
<div class="register">
    <?php
    if (validation_errors()) {
        echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    }
    ?>
    <div class="register-but">
        <form method="post">
            <label>Podaj tytuł</label>
            <input type="text" class="form-control" name='tittle' value = "<?php echo set_value('tittle'); ?>"></br>
            <label>Podaj nazwę autora</label>
            <input type="text" class="form-control" name='author' value = "<?php echo set_value('author'); ?>"></br>
            <label>Dodaj opis</label>
            <textarea class="form-control" rows="3" name='description' ><?php echo set_value('description'); ?> </textarea></br>
            <label>Dodaj swoją ocenę</label>
            <input type="number" min="1" max="10"  class="form-control" name='rating' value = "<?php echo set_value('rating'); ?>"></br>
            <label>Cena</label>
            <input type="number"class="form-control" name='price' value = "<?php echo set_value('price'); ?>"></br>
            <label>Stan</label>
            <select class="form-control" name='condition'>
                <option>Nowa</option>
                <option>Używana</option>
            </select></br>
            <label>Wybierz zdjęcie</label>
            <input type="file" name='photo'>
            </br>
            <input type="submit" class="btn btn-default" value="Dodaj ogłoszenie">
        </form>
        <div class="clearfix"> </div> 
    </div>


</div>
