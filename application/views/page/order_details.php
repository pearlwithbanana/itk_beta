<div class="pag-nav">
    <ul class="p-list">
        <li><a href='<?= base_url() . 'Start' ?>'>Powrót na stronę główną</a></li> &nbsp;&nbsp;/&nbsp;
        <li><a href='<?= base_url() . 'Login' ?>'>Zaloguj</a></li> &nbsp;&nbsp;&nbsp;
    </ul>
</div>
<div class="coats">
    <h3 class="c-head">Podaj dane do wysyłki</h3>
    <p>...aby potwierdzić dane do zamówienia.</p>
</div>
<div class="register">
    <?php
    if (validation_errors()) {
        echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    }
    ?>

    <div class="register-but">
        <form method="post" action="handlers/new_order/order/<?=$link?>"> 
            <div class="register-top-grid">
                <h3>PODAJ DANE DO WYSYŁKI</h3>
                <div>
                    <span>Imię</span>
                    <input name = 'name' value = "<?php
                    if ($this->session->userdata('name')) {
                        echo $this->session->userdata('name');
                    } else {
                        echo set_value('name');
                    }
                    ?>" type = "text">
                </div>
                <div>
                    <span>Nazwisko</span>
                    <input name = 'surname' value = "<?php
                    if ($this->session->userdata('surname')) {
                        echo $this->session->userdata('surname');
                    } else {
                        echo set_value('surname');
                    }
                    ?>" type = "text">
                </div>
                <div>
                    <span>Ulica</span>
                    <input name = 'street' value = "<?php
                    if ($this->session->userdata('street')) {
                        echo $this->session->userdata('street');
                    } else {
                        echo set_value('street');
                    }
                    ?>" type = "text">
                </div>
                <div>
                    <span>Numer lokalu</span>
                    <input name = 'local_number' value = "<?php
                    if ($this->session->userdata('local_number')) {
                        echo $this->session->userdata('local_number');
                    } else {
                        echo set_value('local_number');
                    }
                    ?>" type = "text">
                </div>
                <div>
                    <span>Kod pocztowy</span>
                    <input name = 'post_code' value = "<?php
                    if ($this->session->userdata('post_code')) {
                        echo $this->session->userdata('post_code');
                    } else {
                        echo set_value('post_code');
                    }
                    ?>" type = "text">
                </div>
                <div>
                    <span>Miejscowość</span>
                    <input name = 'place' value = "<?php
                    if ($this->session->userdata('place')) {
                        echo $this->session->userdata('place');
                    } else {
                        echo set_value('place');
                    }
                    ?>" type = "text">
                </div>
                <input type="submit" value="Potwierdź dane"/>
            </div>
        </form>

    </div>

</div>
