<?php $this->load->helper(array('captcha', 'string')); ?>

            <div class="pag-nav">
                <ul class="p-list">
                    <li><a href='<?= site_url('home') ?>'>Powrót na stronę główną</a></li> &nbsp;&nbsp;/&nbsp;
                    <li><a href='<?= site_url('login') ?>'>Zaloguj</a></li> &nbsp;&nbsp;&nbsp;
                </ul>
            </div>
            <div class="coats">
                <h3 class="c-head">Podaj dane, aby zresetować swoje hasło.</h3>
                <p>...i znów cieszyć się z możliwości korzystania z serwisu :)</p>
            </div>
            <div class="register">
                <?php if(validation_errors())
                {echo  '<div class="alert alert-danger">'.validation_errors().'</div>';}
                ?>
                <div class="register-but">
                    <form method="post">
                        <div class="register-top-grid">
                            <h3>Wypełnij poniższe pola.</h3>
                            <div>
                                <span>Podaj adres e-mail, którego użyłeś przy rejestracji.</span>
                                <input id = 'mail' name = 'mail' value = "<?php echo set_value('mail'); ?>" type = "text">
                            </div>
                            <div class = "clearfix"></div>
                            <div id = "emailconfirm"></br></div>

                        </div>
                        <div class = "register-bottom-grid">
                            <BR></BR>
                            
                            <div class = "register-top-grid">
                                <span>Przepisz kod z obrazka</span>
                                <input id = "captcha" type = "text" name = "captcha">
                            </div>
                            <!--CAPTCHA DIV-->
                            <div>
                                <?php
                                $this->load->helper(array('captcha', 'string'));
                                $vals = array(
                                    'word' => random_string('alnum', 8),
                                    'img_path' => './captcha/',
                                    'img_url' => base_url() . 'captcha/',
                                    'font_path' => './system/fonts/texb.ttf',
                                    'img_width' => 300,
                                    'img_height' => 60,
                                    'expiration' => 7200,
                                    'word_length' => 8,
                                    'font_size' => 32,
                                    'img_id' => 'Imageid',
                                    'pool' => '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ',
                                    // White background and border, black text and red grid
                                    'colors' => array(
                                        'background' => array(255, 255, 255),
                                        'border' => array(255, 255, 255),
                                        'text' => array(0, 0, 0),
                                        'grid' => array(255, 40, 40)
                                    )
                                );

                                $cap = create_captcha($vals);
                                $data = array(
                                    'captcha_time' => $cap['time'],
                                    'ip_address' => $this->input->ip_address(),
                                    'word' => $cap['word']
                                );

                                $query = $this->db->insert_string('captcha', $data);
                                $this->db->query($query);
                                echo $cap['image'];
                                echo $cap['word'];
                                ?>
                            </div>
                            <div class="register-but">
                                <input type="submit" value="Zresetuj hasło!"/>
                            </div>
                        </div>

                    </form>
                   <div class="clearfix"> </div> 
                </div>


            </div>
