
<div class="pag-nav">
    <ul class="p-list">
        <li><a href='<?= site_url('home') ?>'>Powrót na stronę główną</a></li> &nbsp;&nbsp;/&nbsp;
        <li class="act">&nbsp;Zaloguj</li>
    </ul>
</div>
<div class="coats">
    <h3 class="c-head">Zaloguj się</h3>
    <p>Jeśli jeszcze nie masz konta, przejdź do sekcji rejestracja.</p>
</div>
<div class="contact-box login-box">
    <?php
    if (validation_errors()) {
        echo '<div class="alert alert-danger">' . validation_errors() . '</div>';
    }
    ?>
    <form method='post'>
        <div class="form">
            <small>Mail</small>
            <div class="text">
                <input name='mail' type="text" />
            </div>
        </div>
        <div class="form">
            <small>Hasło</small>
            <div class="text">
                <input name="password" type="password" />
            </div>
        </div>
        <div class="text">
            <input type="submit" value="Zaloguj" />
        </div>
    </form>
    <div class="text">
        <a href='Przypomnij_haslo'>Nie pamiętam hasła.</a>
    </div>
</div>
<div class="coats login-bot">
    <h3 class="c-head">Jesteś nowy? Kliknij!</h3>
    <div class="reg">
        <a href='<?= site_url('Zarejestruj') ?>'>
            Rejestracja
        </a>
    </div>
</div>

