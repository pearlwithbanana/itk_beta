<?php

class Zarejestruj extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index() {
        //potrzebne składowe
        $this->load->helper(array('form', 'url', 'email'));
        $this->load->library('form_validation');
        $this->load->database();
        //walidacja
        $this->form_validation->set_rules('mail', 'email', 'required|valid_email|is_unique[user.mail]', array('required' => 'Adres %s jest wymagany.', 'is_unique' => 'Taki adres %s już istnieje.', 'valid_email' => 'Wprowadź prawidłowy adres %s.'));
        $this->form_validation->set_rules('password', 'hasło', 'required|min_length[5]|max_length[12]', array('required' => 'Wprowadź %s.', 'min_length' => 'Hasło musi mieć miniumum 5 znaków.', 'max_length' => 'Hasło nie może mieć więcej niż 12 znaków.'));
        $this->form_validation->set_rules('passconf', 'Password confirmation', 'required|matches[password]', array('required' => 'Musisz potwierdzić swoje hasło wpisując je dwa razy.', 'matches' => 'Hasła nie mogą się różnić.'));
        $this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_captcha', array('required' => 'Przepisz kod z obrazka.'));
        if ($this->form_validation->run() == FALSE) {
            //widoki
            $this->load->view('templates/header');
            $this->load->view('page/register');
            $this->load->view('templates/footer');
        } else {
            $this->load->view('templates/header');
            $this->load->view('messages/success_reminding');
            $this->load->view('templates/footer');

            $id = $this -> user_model -> add_user();
            /*
              send_email($mail, $subject, $message);
              $subject = 'Aktywacja konta w serwisie ITK.pl';
              $message = 'AKTYWUJ KONTO JUŻ DZIŚ';
             */
            $mail = $this->input->post('mail');
            $ci = get_instance();
            // mail config
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "poczta.o2.pl";
            $config['smtp_port'] = "587";
            $config['smtp_user'] = "skr14@o2.pl";
            $config['smtp_pass'] = "itk_beta";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $ci->email->initialize($config);
            // po configu
            $this->email->from('skr14@o2.pl', 'ITK.pl');
            $this->email->to($mail);
            $this->email->reply_to($mail, 'ITK.pl');
            $this->email->subject("Aktywacja konta $mail");
            // wiadomośc wysyłamy           
            $dod = md5($id . "kosektopedal");
            $message = "Aby aktywować konto kliknij w poniższy link:<br><a href='http://localhost/beta/index.php/aktywuj/activate/$dod'>Aktywuj konto</a>";
            $this->email->message($message);
            $this->email->send();

        }
    }

    public function captcha() {
        //usuń stare captche
        $expiration = time() - 7200; // dwie godziny
        $this->db->where('captcha_time < ', $expiration)
                ->delete('captcha');
        //sprawdź czy istnieje
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
        $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0) {
            $this->form_validation->set_message('captcha', 'Wprowadź poprawnie kod z obrazka.');
            return false;
        } else {
            return true;
        }
    }

}
