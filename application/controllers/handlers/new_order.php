<?php

class new_order extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
        $this->load->model('User_model');
        $this->load->model('Order_model');
    }

    public function order($link) {
        if ($this->session->userdata('is_logged')) {
            if ($link) {
                $price = $this->Book_model->get_price($link);
                $mail = $this->session->userdata('mail');
                $user_account_status = $this->Book_model->get_users_account_status($mail);
                if ($user_account_status < $price) {
                    $this->load->view('templates/header');
                    $this->load->view('messages/failed_buying');
                    $this->load->view('templates/footer');
                } else {

                    $this->load->library('form_validation');

                    $this->form_validation->set_rules('name', 'imię', 'required|callback_name_PL', array('required' => 'Podaj swoje %s.'));
                    $this->form_validation->set_rules('surname', 'nazwisko', 'required|callback_surname_PL', array('required' => 'Podaj swoje %s.'));
                    $this->form_validation->set_rules('street', 'ulica', 'required|alpha', array('required' => 'Podaj swoją ulicę.', 'text' => 'Podaj poprawną nazwę ulicy!'));
                    $this->form_validation->set_rules('local_number', 'numer lokalu', 'required|alpha_numeric', array('required' => 'Podaj swój %s.', 'alpha_numeric' => 'Podaj %s we właściwym formacie!'));
                    $this->form_validation->set_rules('post_code', 'Kod pocztowy', 'required|callback_post_code_PL', array('required' => 'Podaj swój %s.'));
                    $this->form_validation->set_rules('place', 'miejscowość', 'required|alpha_numeric', array('required' => 'Podaj swoją %s.'));



                    if ($this->form_validation->RUN() === FALSE) {

                        redirect("/Kup_ksiazke/numer/$link");
                    } else {
                        $id_book = $link;
                        $id_user = $this->User_model->get_user_id_by_session($mail);
                        $this->Order_model->new_order($link, $id_user);

                        $name = @$_POST['name'];
                        $surname = @$_POST['surname'];
                        $street = @$_POST['street'];
                        $local_number = @$_POST['local_number'];
                        $post_code = @$_POST['post_code'];
                        $place = @$_POST['place'];

                        $new_account_status = $user_account_status - $price;

                        $this->User_model->update_user_info($name, $surname, $street, $local_number, $post_code, $place, $id_user, $new_account_status);

                        $this->load->view('templates/header');
                        $this->load->view('messages/success_ordering');
                        $this->load->view('templates/footer');
                    }
                }
            }
        } else {

            $this->load->view('templates/header');
            $this->load->view('messages/not_logged_in');
            $this->load->view('templates/footer');
        }
    }

    public function post_code_PL() {
        $post_code = @$_POST['post_code'];
        if (preg_match("/^([0-9]{2})(-[0-9]{3})?$/i", $post_code)) {
            return true;
        } else {
            $this->form_validation->set_message('post_code_PL', 'Podaj kod pocztowy we właściwym formacie!');
            return false;
        }
    }

    public function name_PL() {
        $name = @$POST['name'];

        if (preg_match('/^[\pL \'-]*$/u', $name)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('name_PL', 'Podaj poprawne %s!');
        }
    }

    public function surname_PL() {
        $name = @$POST['surname'];

        if (preg_match('/^[\pL \'-]*$/u', $name)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('name_PL', 'Podaj poprawne %s!');
        }
    }

    public function place_PL() {
        $name = @$POST['place'];

        if (preg_match('/^[\pL \'-]*$/u', $place)) {
            return TRUE;
        } else {
            $this->form_validation->set_message('name_PL', 'Podaj poprawną nazwę miejscowości!');
        }
    }

}
