<?php

class confirm_advertisment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
    }

    public function change($id) {
        $this->Book_model->change_advertisment_activity($id);
    }

}
