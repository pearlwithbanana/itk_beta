<?php

class confirm_order extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Order_model');
    }

    public function change($order_id,$id_book) {
        $this->Order_model->change_order_status($order_id,$id_book);
    }

}
