<?php

class rate_book extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
        $this->load->model('Comment_model');
        $this->load->model('User_model');
    }

    public function book($id_book, $rate) {
        $count = $this->Book_model->get_count_of_rate_info($id_book);
        $actual_rate = $this->Book_model->get_actual_book_rate($id_book);

        $fk_id_user = $this->User_model->get_user_id_by_session($this->session->userdata('mail'));
        $fk_id_book = $id_book;
        $did_user_rate = $this->Comment_model->user_rate($fk_id_user);

        if ($did_user_rate === NULL) {
            $rate_count = $count + 1;
            $rate_sum = $this->Book_model->get_sum_of_rate_info($id_book) + $rate;
        } else {
            $rate_sum = $this->Book_model->get_sum_of_rate_info($id_book) - $did_user_rate + $rate;
            $rate_count = $count;
        }
      
        
        
        $this->Comment_model->update_rate_info($fk_id_user , $fk_id_book, $rate);

        $new_rate = $rate_sum / $rate_count;

        $this->Book_model->update_rate($id_book, $new_rate, $rate_sum, $rate_count);

        echo $this->Book_model->get_actual_book_rate($id_book);
    }

}
