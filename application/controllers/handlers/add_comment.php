<?php

class add_comment extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
        $this->load->model('Comment_model');
        $this->load->model('User_model');
    }

    public function ksiazka($id_book) {

        
        
        $comment = $_POST['comment'];
        $mail = $this->session->userdata('mail');
        $id_user = $this->User_model->get_user_id_by_session($mail);
        $this->Comment_model->new_comment($comment, $id_book, $id_user);
        
         redirect("/Ksiazka/numer/$id_book", 'refresh');

        
    }

}
