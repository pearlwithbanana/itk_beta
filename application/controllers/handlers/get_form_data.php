<?php

class get_form_data extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
    }

    public function tittle($author) {
       
        $author = str_replace('%20',' ',$author);
        
        return $this->Book_model->get_by_author($author);
    }

}
