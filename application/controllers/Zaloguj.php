<?php

class Zaloguj extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index() {

        $this->load->helper(array('form'));
        $this->load->library('form_validation');

        $this->form_validation->set_rules('mail', 'mail', 'required|callback_check_user', array('required' => 'Wprowadź adres %s.'));
        $this->form_validation->set_rules('password', 'hasło', 'required', array('required' => 'Wprowadź %s.'));

        if ($this->session->userdata('is_logged') == TRUE) {
            $this->load->view('templates/header');
            $this->load->view('page/profile');
            $this->load->view('templates/footer');
        } else {
            if ($this->form_validation->run() == FALSE) {
                //widoki
                $this->load->view('templates/header');
                $this->load->view('page/login');
                $this->load->view('templates/footer');
            } else {


                $login = $_POST['mail'];
                $user = $_POST['mail'];

                $account_type = $this->user_model->get_user_account_type($login);

                $user_data = $this->user_model->get_user_info($user);
               
                $data = array('account_type' => $account_type, 'mail' => $login, 'is_logged' => TRUE,
                    'name' => $user_data['name'], 'surname' => $user_data['surname'],
                    'street' => $user_data['street'],'local_number'=>$user_data['local_number'],
                    'post_code'=>$user_data['post_code'],'place'=>$user_data['place']);
                
                $this->session->set_userdata($data);
                if ($account_type === 'user') {
                    $this->load->view('templates/header');
                    $this->load->view('page/profile');
                    $this->load->view('templates/footer');
                } else {
                    $this->load->view('templates/header');
                    $this->load->view('page/admin_panel');
                    $this->load->view('templates/footer');
                }
            }
        }
    }

    public function check_user() {
        $mail = $_POST['mail'];
        $password = $_POST['password'];
        $data = array(
            'mail' => $mail,
            'haslo' => md5($password),
        );
        $result = $this->db->get_where('user', $data);
        if ($result->num_rows() > 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_user', 'Adres e-mail lub hasło są nieprawidłowe.');
            return FALSE;
        }
    }

}
