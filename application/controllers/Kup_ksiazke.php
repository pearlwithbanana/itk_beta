<?php

class Kup_ksiazke extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
        $this->load->model('User_model');
        $this->load->model('Order_model');
    }

    public function numer_ksiazki($link) {
        
        $this->load->library('form_validation');
        
        $data['link'] = $link;
        
        $this->load->view('templates/header');
        $this->load->view('page/order_details',$data);
        $this->load->view('templates/footer');
    }

}
