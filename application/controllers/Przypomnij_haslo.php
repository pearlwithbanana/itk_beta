<?php

class Przypomnij_haslo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index() {
        $this->load->helper('captcha', 'form');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('mail', 'email', 'required|callback_check_user', array('required' => 'Wprowadź adres %s.'));
        $this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_captcha', array('required' => 'Przepisz kod z obrazka.'));
        if ($this->form_validation->run() == FALSE) {
            //widoki
            $this->load->view('templates/header');
            $this->load->view('page/password_reminder');
            $this->load->view('templates/footer');
        } else {
            $this->load-> view('templates/header');
            $this->load->view('messages/success_reminding');
            $this->load-> view('templates/footer');
            /*
              send_email($mail, $subject, $message);
              $subject = 'Aktywacja konta w serwisie ITK.pl';
              $message = 'AKTYWUJ KONTO JUŻ DZIŚ';
             */
            
            $mail = $this->input->post('mail');
            
            
            
            $ci = get_instance();
            // mail config
            $ci->load->library('email');
            $config['protocol'] = "smtp";
            $config['smtp_host'] = "poczta.o2.pl";
            $config['smtp_port'] = "587";
            $config['smtp_user'] = "skr14@o2.pl";
            $config['smtp_pass'] = "itk_beta";
            $config['charset'] = "utf-8";
            $config['mailtype'] = "html";
            $config['newline'] = "\r\n";
            $ci->email->initialize($config);
            // po configu
            $this->email->from('skr14@o2.pl', 'ITK.pl');
            $this->email->to($mail);
            $this->email->reply_to($mail, 'ITK.pl');
            $this->email->subject("Przypomnienie hasła w serwisie ITK.pl dla $mail");
            // wiadomośc wysyłamy
            $id = $this->user_model->get_id();
            //$id = $this -> user_model -> add_user();
            $dod = md5($id.'kosektopedalhaslo');//date('d-m-y')
            $message = "Cześć!</br>Doszły mnie słuchy, że zapomniałeś hasła do swojego konta i chciałbyś je zresetować. Nic prostszego! Po prostu kliknij w poniższy link:</br>
                    <br><a href='http://localhost/beta/index.php/Zresetuj/reset_password/$dod'>O, właśnie ten :)</a>";
            $this->email->message($message);
            $this->email->send();
            //dodaj hasz do bazy
            
            $this->user_model->generate_hash($dod,$mail);
            //$dod = md5($dod . date('d-m-y'));
            //$this->user_model->generate_unique_hash($dod,$mail);
        }
    }

    public function check_user() {
        $mail = $_POST['mail'];
        $data = array(
            'mail' => $mail,
        );
        $result = $this->db->get_where('user', $data);
        if ($result->num_rows() > 0) {
            return TRUE;
        } else {
            $this->form_validation->set_message('check_user', 'Podany adres e-mail nie istnieje!');
            return FALSE;
        }
    }

    public function captcha() {
        // First, delete old captchas
        $expiration = time() - 7200; // Two hour limit
        $this->db->where('captcha_time < ', $expiration)
                ->delete('captcha');

// Then see if a captcha exists:
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
        $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0) {
            $this->form_validation->set_message('captcha', 'Wprowadź poprawnie kod z obrazka.');
            return false;
        } else {
            return true;
        }
    }

}
