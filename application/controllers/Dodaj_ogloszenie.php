<?php

class Dodaj_ogloszenie extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
    }

    public function index() {
        $this->load->library('upload');
        $this->load->library('form_validation');




        if ($this->session->userdata('is_logged') == FALSE) {
            $this->load->view('templates/header');
            $this->load->view('messages/not_logged_in');
            $this->load->view('templates/footer');
        } else {

            $this->form_validation->set_rules('tittle', 'tytuł', 'required|min_length[5]|max_length[48]', array('required' => 'Podaj %s', 'min_length' => 'Tytuł nie może być krótszy niż 5 znaków!', 'max_length' => 'Tytuł może mieć maksymalnie 48 znaków. '));
            //$this->form_validation->set_rules('','','',array(''=>''));
            $this->form_validation->set_rules('author', 'autora', 'required|min_length[2]|max_length[32]', array('required' => 'Podaj imię i nazwisko %s', 'min_length' => 'Nazwa %s nie może być krótsza niż 2 znaków!', 'max_length' => 'Nazwa %s nie może mieć więcej niż 32 znaków!'));
            $this->form_validation->set_rules('description', 'opis', 'required|min_length[16]|max_length[2048]', array('required' => 'Podaj opis przedmiotu', 'min_length' => 'Opis nie może być krótsza niż 16 znaków!', 'max_length' => 'Opis nie może mieć więcej niż 2048 znaków!'));
            $this->form_validation->set_rules('rating', 'ocena', 'numeric', array('numeric' => 'Podaj prawidłową wartość z przedziału między 1 a 10!'));
            $this->form_validation->set_rules('price', 'cena', 'required|numeric', array('required' => 'Podaj cenę!', 'numeric' => 'Cena musi być liczbą!'));
            $this->form_validation->set_rules('condition', 'stan', 'required', array('required' => 'Określ %s książki!'));
            //$this->form_validation->set_rules('photo','plik','required|callback_upload_photo',array('required'=>'Dodaj zdjęcie okładki!'));
            if ($this->form_validation->run() == FALSE) {

                $this->load->view('templates/header');
                $this->load->view('page/add_advertisment');
                $this->load->view('templates/footer');
            } else {
                if ($this->session->userdata('is_logged')) {
                    $mail = $this->session->userdata('mail');
                    $tittle = $_POST['tittle'];
                    $author = $_POST['author'];
                    $description = $_POST['description'];
                    $rating = $_POST['rating'];
                    $price = $_POST['price'];
                    $condition = $_POST['condition'];
                    $data = array(
                        'id_book' => '',
                        'grade' => $rating,
                        'image_path' => '',
                        'tittle' => $tittle,
                        'author' => $author,
                        'condition' => $condition,
                        'description' => $description,
                        'price' => $price,
                        'seller' => $mail,
                        'fk_id_user' => '',
                        'fk_id_type' => ''
                        
                    );

                    $this->Book_model->new_advertisment($data);
                    $this->load->view('templates/header');
                    $this->load->view('messages/success_advertisment_adding');
                    $this->load->view('templates/footer');
                    $this->upload->data();
                }
            }
        }
    }

    public function upload_photo() {

        $config['upload_path'] = './images/';
        $config['allowed_types'] = 'image/jpeg';
        $config['max_size'] = '1024';
        $config['max_width'] = '2048';
        $config['max_height'] = '1536';

        if (!$this->upload->do_upload()) {
            return $this->form_validation->set_message('photo', 'Obrazek nie może mieć więcej niż 1MB. Maksymalne dozwolone rozmiary to 2048x1536.');
            return FALSE;
        } else {

            return TRUE;
        }
    }

}
