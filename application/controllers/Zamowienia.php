<?php

class Zamowienia extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Order_model');
    }

    public function Strona($offset = 0) {

        if ($this->session->userdata('account_type') == "admin") {

            $this->load->library('pagination');

            $rows = $this->Order_model->record_count_for_send();

            $config = array();
            $config['base_url'] = 'Zamowienia/strona';
            $config['per_page'] = 10;
            $config['first_tag_open'] = '<li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';
            $config['cur_tag_open'] = '<li><a><b>';
            $config['cur_tag_close'] = '</b></a></li>';
            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';
            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';
            $config['last_tag_open'] = '<li>';
            $config['last_tag_close'] = '</li>';
            $config['next_link'] = 'Następna';
            $config['prev_link'] = 'Poprzednia';
            $config['last_link'] = 'Ostatnia';
            $config['first_link'] = 'Pierwsza';
            $config['num_links'] = 8;

            $config['uri_segment'] = 3;
            $config['total_rows'] = $rows;
            $this->pagination->initialize($config);

            $data['links'] = $this->pagination->create_links();
            
            $data['result'] = $this->Order_model->fetch_orders_for_admin($config["per_page"], $offset);

            $this->load->view('templates/header');
            $this->load->view('page/admin_books_sender', $data);
            $this->load->view('templates/footer');
        } else {
            show_404();
        }
    }

}
