<?php

class Wyloguj extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
    }
    public function index()
    {
        $this->session->unset_userdata('is_logged');
        $this->session->unset_userdata('account_type');
        
        $this->load->view('templates/header');
        $this->load->view('page/index');
        $this->load->view('templates/footer');
    }
}

