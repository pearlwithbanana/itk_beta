<?php

Class Ksiazka extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
        $this->load->model('Comment_model');
        $this->load->model('User_model');
    }

    public function numer($id_book) {


        $data['link'] = $id_book;
        $this->load->view('templates/header');
        $this->load->view('page/single_book_view', $data);
        $this->load->view('templates/footer');
    }

}
