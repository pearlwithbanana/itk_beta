<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Zresetuj extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function reset_password($dod) {
        $dod = md5($dod . date('d-m-y'));

        if ($this->user_model->reset_user_password($dod) == FALSE) {
            //echo $dod.'</br>';
            $this->user_model->reset_user_password($dod);
            $this->load->view('templates/header');
            $this->load->view('messages/failed_reminding');
            $this->load->view('templates/footer');
        } else {
            $this->load->library('form_validation');

            $this->form_validation->set_rules('password', 'hasło', 'required|min_length[5]|max_length[12]', array('required' => 'Wprowadź %s.', 'min_length' => 'Hasło musi mieć miniumum 5 znaków.', 'max_length' => 'Hasło nie może mieć więcej niż 12 znaków.'));
            $this->form_validation->set_rules('passconf', 'Password confirmation', 'required|matches[password]', ['required' => 'Musisz potwierdzić swoje hasło wpisując je dwa razy.', 'matches' => 'Hasła nie mogą się różnić.']);
            // widok jestaktywowany.php
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('templates/header');
                $this->load->view('messages/success_password_reseting');
                $this->load->view('templates/footer');
            } else {
                $password = $_POST['password'];
                $this->user_model->new_password($password, $dod);
                $this->load->view('templates/header');
                $this->load->view('messages/done_reseting_password');
                $this->load->view('templates/footer');
                $this->user_model->reset_activation_mail($dod);
            }
        }
    }

}
