<?php

class Nowe_haslo extends CI_Controller {

    public function index() {
        $this->load->library('form_validation');
        

        $this->form_validation->set_rules('password', 'hasło', 'required|min_length[5]|max_length[12]', array('required' => 'Wprowadź %s.', 'min_length' => 'Hasło musi mieć miniumum 5 znaków.', 'max_length' => 'Hasło nie może mieć więcej niż 12 znaków.'));
        $this->form_validation->set_rules('passconf', 'Password confirmation', 'required|matches[password]', array('required' => 'Musisz potwierdzić swoje hasło wpisując je dwa razy.', 'matches' => 'Hasła nie mogą się różnić.'));

        if ($this->form_validation->run() == FALSE) {
            //widoki
            $this->load->view('templates/header');
            $this->load->view('page/sucess_reminding');
            $this->load->view('templates/footer');
        } else {
            $this->load->view('templates/header');
            $this->load->view('messages/success_password_reseting');
            $this->load->view('templates/footer');
        }
    }

}
