<?php

class Doladuj_konto extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    public function index() {
        $this->load->library('form_validation');

        $this->form_validation->set_rules('code', 'kod', 'required|exact_length[4]', array('exact_length' => 'Podaj prawidłowy czterocyfrowy kod!'));
        $this->form_validation->set_rules('captcha', 'Captcha', 'required|callback_captcha', array('required' => 'Przepisz kod z obrazka.'));
        if ($this->session->userdata('is_logged')) {
            if ($this->form_validation->run() == FALSE) {
                $this->load->view('templates/header');
                $this->load->view('/page/recharge_your_account');
                $this->load->view('templates/footer');
            } else {
                $mail = $this->session->userdata('mail');
                $this->user_model->recharge_account($mail);
                $this->load->view('templates/header');
                $this->load->view('messages/success_recharging');
                $this->load->view('templates/footer');
            }
        } else {
            $this->load->view('templates/header');
            $this->load->view('/messages/not_logged_in');
            $this->load->view('templates/footer');
        }
    }

    public function captcha() {
        // First, delete old captchas
        $expiration = time() - 7200; // Two hour limit
        $this->db->where('captcha_time < ', $expiration)
                ->delete('captcha');

// Then see if a captcha exists:
        $sql = 'SELECT COUNT(*) AS count FROM captcha WHERE word = ? AND ip_address = ? AND captcha_time > ?';
        $binds = array($_POST['captcha'], $this->input->ip_address(), $expiration);
        $query = $this->db->query($sql, $binds);
        $row = $query->row();

        if ($row->count == 0) {
            $this->form_validation->set_message('captcha', 'Wprowadź poprawnie kod z obrazka.');
            return false;
        } else {
            return true;
        }
    }

}
