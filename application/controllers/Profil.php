<?php

class profil extends CI_Controller {

    public function index() {
        if ($this->session->userdata('is_logged') == TRUE) {
            if ($this->session->userdata('account_type') === 'user') {
                $this->load->view('templates/header');
                $this->load->view('page/profile');
                $this->load->view('templates/footer');
            }
            else
            {
                $this->load->view('templates/header');
                $this->load->view('page/admin_panel');
                $this->load->view('templates/footer');
            }
        } else {
            $this->load->view('templates/header');
            $this->load->view('messages/not_logged_in');
            $this->load->view('templates/footer');
        }
    }

}
