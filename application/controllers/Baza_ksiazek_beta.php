<?php

class Baza_ksiazek_beta extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
    }

    public function index() {
        
        $this->load->library('pagination');
        $config = array();
        $config['base_url'] = base_url() . 'Baza_ksiazek/index';
        $config['total_rows'] = $this->Book_model->record_count();
        $config['per_page'] = 10;
        $config['first_tag_open'] = '<li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Następna';
        $config['prev_link'] = 'Poprzednia';
        $config['last_link'] = 'Ostatnia';
        $config['first_link'] = 'Pierwsza';
        $config['num_links'] = 8;
        $config['uri_segment'] = 3;

        if ($_POST == FALSE) {
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $results = $this->Book_model->fetch_books($config["per_page"], $page);
            $data["results"] = $this->Book_model->fetch_books($config["per_page"], $page);
            $data["links"] = $this->pagination->create_links();
           
            $this->load->view('templates/header');
            $this->load->view('page/books_browser', $data);
            $this->load->view('templates/footer');
        } else {
            $syntax = $this->search();
            $this->session->userdata('syntax',$syntax);
            $this->pagination->initialize($config);
            $page = ($this->uri->segment(3)) ? $this->uri->segment(3) : 0;
            $results = $this->Book_model->fetch_books($config["per_page"], $page);
            $data["results"] = $this->Book_model->fetch_books($config["per_page"], $page, $syntax);
            $data["links"] = $this->pagination->create_links();
            if (length($data['results']) > 0) {
                foreach ($data['results'] as $row) {
                    $condition = $row->condition;
                    if ($condition == 0) {
                        $condition = 'Używana';
                    } else {
                        $condition = 'Nowa';
                    }
                    echo "<tr>";
                    echo "<td><h3>" . $row->grade . "</h3></td>";
                    echo "<td><img width='50'heigth='75' src='" . $row->image_path . "'/></td>";
                    echo "<td>" . $row->tittle . "</td>";
                    echo "<td>" . $row->author . "</td>";
                    echo "<td>" . $condition . "</td>";
                    echo "<td>" . $row->price . " ,-</td>";
                    echo "<td>" . $row->seller . "</td>";
                    echo "</tr>";
                    echo $syntax;
                }
            }
            else
            {
                echo "Nie ma takiej ksiunszki";
            }
        }
    }

    public function search() {
        $author = @$_POST['author'];
        $tittle = @$_POST['tittle'];
        $minPrice = floatval(@$_POST['minPrice']);
        $maxPrice = floatval(@$_POST['maxPrice']);
        if ($minPrice < $maxPrice) {
            $minPrice = $minPrice;
            $maxPrice = $maxPrice;
        } else {
            $minPrice_backup = $minPrice;
            $minPrice = $maxPrice;
            $maxPrice = $minPrice_backup;
        }
        $minRate = floatval(@$_POST['minRate']);
        $maxRate = floatval(@$_POST['maxRate']);
        if ($minRate < $maxRate) {
            $minRate = $minRate;
            $maxRate = $maxRate;
        } else {
            $minRate_backup = $minRate;
            $minRate = $maxRate;
            $maxRate = $minRate_backup;
        }
        $condition = @$_POST['condition'];
        $order = @$_POST['order'];

        $starting_query = "SELECT * FROM book where";
        
        $author = $author == 'Autor' ? ' author != "NULL"' : $author = ' author LIKE "%' . $author . '%"';
        $tittle = $tittle == 'Tytuł' ? ' AND tittle !="NULL"' : $tittle = ' AND tittle LIKE"%' . $tittle . '%"';
        $minPrice = $minPrice == 'Cena od' ? ' AND price BETWEEN 0' : $minPrice = ' AND price BETWEEN ' . $minPrice;
        $maxPrice = $maxPrice == 'Cena do' ? ' AND 99999999999999999' : $maxPrice = ' AND ' . $maxPrice;
        $minRate = $minRate == 'Ocena od' ? ' AND grade BETWEEN 0' : $minRate = ' AND grade BETWEEN ' . $minRate;
        $maxRate = $maxRate == 'Ocena do' ? ' AND 11' : $maxRate = ' AND ' . $maxRate;
        if ($order == 'Sortuj według') {
            $order = ' ORDER BY grade DESC';
        } else if ($order == 'Cena: rosnąco') {
            $order = ' ORDER BY price ASC';
        } else if ($order == 'Cena: malejąco') {
            $order = ' ORDER BY price DESC';
        } else {
            $order = ' ORDER BY price ASC';
        }
        if ($condition == 'Stan') {
            $condition = ' AND "condition" != "NULL"';
        } else if ($condition == 'Nowa') {
            $condition = ' AND "condition" = 1';
        } else {
            $condition = ' AND "condition" = 0';
        }
        //$query = 'SELECT * FROM book where author != "NULL" AND tittle !="NULL" AND price BETWEEN 0 AND 99999999999999999 AND grade BETWEEN 0 AND 11 AND "condition" != "NULL" ORDER BY price ASC';

        $query = $starting_query . $author . $tittle . $minPrice . $maxPrice . $minRate . $maxRate . $condition . $order;

        echo $author;
        return $query;
    }

}
