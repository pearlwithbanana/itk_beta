<?php

class Start extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
    }

    public function index() {
        $this->load->view('templates/header');
        $this->load->view('page/index');
        $this->load->view('templates/footer');
    }

}
