<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Aktywuj extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    function activate($dod) {
        if ($this->user_model->aktywuj($dod) == TRUE) {
            // widok jestaktywowany.php
            $this->load->view('templates/header');
            $this->load->view('messages/success_activation');
            $this->load->view('templates/footer');
            
        }
        else
        {   
            $this->load->view('templates/header');
            $this->load->view('messages/failed_activation');
            $this->load->view('templates/footer');
        }
    }

}
