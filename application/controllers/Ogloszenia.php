<?php

class Ogloszenia extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Book_model');
    }

    public function index() {
        //tu napisz domyślny widok wszystkich książek w przyszłości
    }

    public function Strona($offset = 0) {

        $this->load->library('form_validation');
        $this->load->library('pagination');
        
        if ($_POST) {
            
            $author = @$_POST['author'];
            $tittle = @$_POST['tittle'];
            $minPrice = floatval(@$_POST['minPrice']);
            $maxPrice = floatval(@$_POST['maxPrice']);
            $minRate = floatval(@$_POST['minRate']);
            $maxRate = floatval(@$_POST['maxRate']);
            $condition = @$_POST['condition'];
            $order = @$_POST['order'];
            $fields = array();
            $fields['author'] = $author;
            $fields['tittle'] = $tittle;
            $fields['minPrice'] = $minPrice;
            $fields['maxPrice'] = $maxPrice;
            $fields['minRate'] = $minRate;
            $fields['maxRate'] = $maxRate;
            $fields['condition'] = $condition;
            
             if ($order == 'Sortuj według') {
               $fields['order'] = 'Sortuj według';
            } else if ($order == 'Cena: rosnąco') {
                $fields['order'] = 'Cena rosnąco';
            } else if ($order == 'Cena: malejąco') {
                $fields['order'] = 'Cena: malejąco';
            } else {
                $fields['order'] = 'Cena: malejąco';
            }
            
            $this -> session -> set_userdata('fields',$fields);
            
            $searching = array();
            $searching['author'] = $author == 'Autor' ? ' author != "NULL"' : $author = ' author LIKE "%' . $author . '%"';
            $searching['tittle'] = $tittle == 'Tytuł' ? ' AND tittle !="NULL"' : $tittle = ' AND tittle LIKE"%' . $tittle . '%"';
            $searching['minPrice'] = $minPrice == 'Cena od' ? ' AND price BETWEEN 0' : $minPrice = ' AND price BETWEEN ' . $minPrice;
            $searching['maxPrice'] = $maxPrice == 'Cena do' ? ' AND 99999999999999999' : $maxPrice = ' AND ' . $maxPrice;
            $searching['minRate'] = $minRate == 'Ocena od' ? ' AND grade BETWEEN 0' : $minRate = ' AND grade BETWEEN ' . $minRate;
            $searching['maxRate'] = $maxRate == 'Ocena do' ? ' AND 11' : $maxRate = ' AND ' . $maxRate;
            if ($order == 'Sortuj według') {
                $searching['order'] = ' ORDER BY grade DESC';
            } else if ($order == 'Cena: rosnąco') {
                $searching['order'] = ' ORDER BY price ASC';
            } else if ($order == 'Cena: malejąco') {
                $searching['order'] = ' ORDER BY price DESC';
            } else {
                $searching['order'] = ' ORDER BY price ASC';
            }
            if ($condition == 'Stan') {
                $searching['condition'] = ' AND "condition" != "NULL"';
            } else if ($condition == 'Nowa') {
                $searching['condition'] = ' AND "condition" = 1';
            } else {
                $searching['condition'] = ' AND "condition" = 0';
            }


            //$this->session->set_userdata('query', $query);
            /*
             * 1. Zamień 46 linię na $data['result'] $this->Book_model->fetchbooks 
             * 2. w books_browser zapamiętaj jako value aktualnie wybrane parametry
             * 3. PAMIĘTAJ O ZMIANIE WSZYSTKICH MIEJSC, GDZIE KORZYSTASZ Z COOKIE!
             */
            //$config['total_rows'] = $this->Book_model->record_count($query);
            //$this->pagination->initialize($config);
            $this->session->set_userdata('searching', $searching);
            $rows = $this->Book_model->record_count();
         
        } 
        else if($this->session->userdata('searching'))
        {
            $rows = $this->Book_model->record_count();
        }
        else {
            $this->session->set_userdata('searching', FALSE);
            $rows = $this->Book_model->record_count();
        }
        $config = array();
        $config['base_url'] = 'Ogloszenia/strona';
        $config['per_page'] = 10;
        $config['first_tag_open'] = '<li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = '<li><a><b>';
        $config['cur_tag_close'] = '</b></a></li>';
        $config['prev_tag_open'] = '<li>';
        $config['prev_tag_close'] = '</li>';
        $config['next_tag_open'] = '<li>';
        $config['next_tag_close'] = '</li>';
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';
        $config['next_link'] = 'Następna';
        $config['prev_link'] = 'Poprzednia';
        $config['last_link'] = 'Ostatnia';
        $config['first_link'] = 'Pierwsza';
        $config['num_links'] = 8;
        //$config['query_string_segment'] = $page;
        // wzór na uri_segment (offset)
        $config['uri_segment'] = 3;
        $config['total_rows'] = $rows;
        $this->pagination->initialize($config);

        $data['links'] = $this->pagination->create_links();
       
        $data['result'] = $this->Book_model->fetch_books_for_remove($config["per_page"], $offset);
        //var_dump($data['result']);
        //echo $page;
        //echo $this->pagination->create_links();
        $this->load->view('templates/header');
        $this->load->view('page/admin_books_remover', $data);
        $this->load->view('templates/footer');
    }

}
