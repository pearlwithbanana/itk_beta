<?php

class User_model extends CI_Model {

    public function add_user() {
        $mail = $_POST['mail'];
        $password = $_POST['password'];
        $data = array(
            'id_user' => '',
            'mail' => $mail,
            'haslo' => md5($password),
            'imie' => '',
            'nazwisko' => '',
            'fk_id_book' => ''
        );
        $this->db->insert('user', $data);
        $mail = $this->input->post('mail');
        $query = $this->db->query("select id_user from user where mail = '$mail'");

        foreach ($query->result() as $row) {
            return $row->id_user;
        }
    }

    public function get_id() {

        $mail = $this->input->post('mail');

        $query = $this->db->query("select id_user from user where mail = '$mail'");

        foreach ($query->result() as $row) {
            return $row->id_user;
        }
    }

    public function reset_user_password($hash) {

        $this->db->select('mail')->from('user')->where('activation_mail', $hash);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function reset_activation_mail($hash) {
        $data = array('activation_mail' => '');
        $this->db->where('activation_mail', $hash);
        $this->db->update('user', $data);
    }

    function aktywuj($hash) {
        $query = $this->db->get("user");
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $rows) {
                if (md5($rows->id_user . "kosektopedal") == $hash AND $rows->Active == 0) {
                    $data1 = array('active' => 1);
                    $this->db->where("id_user", $rows->id_user);
                    $this->db->update('user', $data1);
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    public function new_password($password, $dod) {
        $password = md5($password);
        $data = array(
            'haslo' => $password
        );

        $this->db->where('activation_mail', $dod);
        $this->db->update('user', $data);
    }

    public function generate_hash($hash, $mail) {
        $hash = md5($hash . date('d-m-y'));
        $data = array('activation_mail' => $hash);
        $this->db->where('mail', $mail);
        $this->db->update('user', $data);
    }

    public function generate_unique_hash($hash, $mail) {
        $data = array('activation_mail_hash' => $hash);
        $this->db->where('mail', $mail);
        $this->db->update('user', $data);
    }

    public function recharge_account($mail) {
        //this->db->where('mail', $mail);
        $this->db->select('account_status');
        $this->db->where('mail', $mail);
        $query = $this->db->get('user');

        foreach ($query->result() as $row) {
            $old_val = $row->account_status;
        }

        $new_val = $old_val + 10.98;

        $data = array(
            'account_status' => $new_val
        );


        $this->db->where('mail', $mail);
        $this->db->update('user', $data);
    }

    public function get_user_account_status($mail) {
        $this->db->where('mail', $mail);
        $this->db->select('account_status');
        echo $this->db->get('user');
    }

    public function get_user_account_type($user = 'random') {

        $this->db->where('mail', $user);
        $this->db->select('account_type');
        $query = $this->db->get('user');

        foreach ($query->result() as $row) {
            return $row->account_type;
        }
    }

    public function update_user_info($name, $surname, $street, $local_number, $post_code, $place, $user_id, $new_account_status) {
        $data = array('imie' => $name, 'nazwisko' => $surname, 'ulica' => $street, 'numer_domu' => $local_number, 'kod_pocztowy' => $post_code, 'miejscowosc' => $place, 'account_status' => $new_account_status);
        $this->db->where('id_user', $user_id);
        $this->db->update('user', $data);
    }

    public function get_user_id_by_session($mail) {
        $this->db->where('mail', $mail);
        $this->db->select('id_user');
        $query = $this->db->get('user');

        foreach ($query->result() as $row) {
            return $row->id_user;
        }
    }

    public function get_user_info($mail) {
        $data = array();
        $query = "SELECT * FROM user WHERE mail = '$mail'";
        $query = $this->db->query($query);
        foreach ($query->result() as $row) {
            $data['name'] = $row->imie;
            $data['surname'] = $row->nazwisko;
            $data['street'] = $row->ulica;
            $data['local_number'] = $row->numer_domu;
            $data['post_code'] = $row->kod_pocztowy;
            $data['place'] = $row->miejscowosc;
            
        }
        //var_dump($user);
        return $data;
    }

}
