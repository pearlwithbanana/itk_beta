<?php

class Comment_model extends CI_Model {

    public function get_comment_info($id_book) {
        $query = "SELECT user.mail, comment.comment FROM user, comment 
                  WHERE comment.fk_id_book = $id_book
                  AND comment.fk_id_user = user.id_user
                ";
        $query = $this->db->query($query);

        foreach ($query->result() as $row) {
            echo"
            <div class='comment'>
            <div class = 'well'>
            <div class = 'media'
            <div class = 'media-body'>
            <h4 class = 'media-heading'>$row->mail</br>
            <small>O tej książce:</small>
            </h4>
            $row->comment
            </div>
            </div>
            </div>
             ";
        }
    }
    public function new_comment($comment , $id_book ,$id_user)
    {
        $data = array('id_comment'=>'','comment'=>$comment,'fk_id_book'=>$id_book, 'fk_id_user'=>$id_user);
        $this->db->insert('comment',$data);
    }
    public function user_rate($fk_id_user)
    {
        $this->db->where('fk_id_user',$fk_id_user);
        $this->db->select('rate');
        $query = $this->db->get('comment');
        
        foreach($query->result() as $row)
        {
            return $row->rate;
        }
    }
    public function update_rate_info($fk_id_user,$fk_id_book,$rate)
    {
        $data = array('rate' => $rate,'fk_id_book'=>$fk_id_book);
        
        $this->db->where('fk_id_user',$fk_id_user);
        $this->db->update('comment',$data);
    }
}
