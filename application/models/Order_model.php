<?php

class Order_model extends CI_Model {

    public function new_order($id_book, $user_id) {
        $data = array('order_id' => '', 'fk_id_user' => $user_id, 'fk_id_book' => $id_book);
        $this->db->insert('orders', $data);
        $data = array('active' => 'aktualne');
        $this->db->where('id_book', $id_book);
        $this->db->update('book', $data);
    }

    public function fetch_orders_for_admin($limit, $start) {

        $query = "SELECT orders.order_id, orders.fk_id_book, user.imie, user.nazwisko, user.ulica, user.numer_domu, user.kod_pocztowy, user.miejscowosc 
                FROM orders, user 
                WHERE orders.fk_id_user = user.id_user AND order_status = 'niewyslano'
                LIMIT $limit OFFSET $start";
        $query = $this->db->query($query);

        $data = array();
        if ($query->num_rows() > 1) {
            foreach ($query->result() as $row) {

                $order_id = $row->order_id;
                $id_book = $row->fk_id_book;
                $data[] = "<tr>" .
                        "<td><h4>" . $row->order_id . "</h3></td>" .
                        "<td>" . $row->fk_id_book . "</td>" .
                        "<td>" . $row->imie . "</td>" .
                        "<td>" . $row->nazwisko . "</td>" .
                        "<td>" . $row->ulica . "</td>" .
                        "<td>" . $row->numer_domu . "</td>" .
                        "<td>" . $row->kod_pocztowy . "</td>" .
                        "<td>" . $row->miejscowosc . "</td>" .
                        "<td>"
                        . "<div class='confirm_order' id='$order_id'>"
                        . "<div class='btn btn-info' id='$id_book'>"
                        . "Oznacz jako wysłany"
                        . "</div>"
                        . "</div>" .
                        "</tr>";
            }



            return $data;
        } else if ($query->num_rows() === 1) {
            foreach ($query->result() as $row) {
                $order_id = $row->order_id;
                $id_book = $row->fk_id_book;
                "<tr>" .
                        "<td><h4>" . $row->order_id . "</h3></td>" .
                        "<td>" . $row->fk_id_book . "</td>" .
                        "<td>" . $row->imie . "</td>" .
                        "<td>" . $row->nazwisko . "</td>" .
                        "<td>" . $row->ulica . "</td>" .
                        "<td>" . $row->numer_domu . "</td>" .
                        "<td>" . $row->kod_pocztowy . "</td>" .
                        "<td>" . $row->miejscowosc . "</td>" .
                        "<td>"
                        . "<div class='confirm_order' id='$order_id'>"
                        . "<div class='btn btn-info' id='$id_book'>"
                        . "Oznacz jako wysłany"
                        . "</div>"
                        . "</div>" .
                        "</tr>";
                $data['links'] = $this->pagination->create_links();
            }
        } else {
            return "Nie ma żadnych aktualnych zamówień.";
        }
    }

    public function record_count_for_send() {
        $this->db->where('order_status', 'niewyslano');
        $query = $this->db->get('orders');
        return $query->num_rows();
    }

    public function change_order_status($order_id, $id_book) {
        //UPDATE ORDERS SET `order_status` = 'wyslano' WHERE `order_id` = 2;
        $query = "UPDATE orders SET order_status = 'wyslano' WHERE order_id = $order_id;";
        $this->db->query($query);
        $query = "UPDATE book SET active = 'nieaktualne' WHERE id_book = '$id_book'";
        $this->db->query($query);
    }

}
