<?php

class Book_model extends CI_Model {

    public function record_count() {
        $query = $this->session->userdata('searching');
        if ($query) {
            $searching = $this->session->userdata('searching');
            $query = $searching['author'] . $searching['tittle'] . $searching['minPrice'] . $searching['maxPrice'] . $searching['minRate'] . $searching['maxRate'] . $searching['condition'] . $searching['order'];
            $query = $this->db->query("SELECT * FROM book WHERE active = 'aktywowano' AND " . $query);
            $count = $query->num_rows();
            return $count;
        } else {
            $query = $this->db->query("SELECT * FROM book WHERE active = 'aktywowano'");
            $count = $query->num_rows();
            return $count;
        }
    }

    public function record_count_for_admin() {

        $query = $this->db->query("SELECT * FROM book WHERE active = 'nieaktywowano'");
        return $query->num_rows();
    }

    public function fetch_books($limit, $start) {
        $query = $this->session->userdata('searching');
        if ($query) {
            $searching = $this->session->userdata('searching');
            $query = $searching['author'] . $searching['tittle'] . $searching['minPrice'] . $searching['maxPrice'] . $searching['minRate'] . $searching['maxRate'] . $searching['condition'] . $searching['order'];
            $query = "SELECT * FROM book WHERE active= 'aktywowano' AND " . $query;
        } else {
            $query = "SELECT * FROM book WHERE active = 'aktywowano' ORDER BY price ASC";
        }
        //$query = $query . " LIMIT $limit OFFSET $start";
        $query = $this->db->query($query . " LIMIT $limit OFFSET $start");
        $data = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $condition = $row->condition;
                if ($condition == 0) {
                    $condition = 'Używana';
                } else {
                    $condition = 'Nowa';
                }

                $link = $row->id_book;
                $data[] = "<tr>" .
                        "<td><h3>" . $row->grade . "</h3></td>" .
                        "<td><img width='50'heigth='75' src='" . $row->image_path . "'/></td>" .
                        "<td><a href='Ksiazka/numer/$link'>" . $row->tittle . "</a></td>" .
                        "<td>" . $row->author . "</td>" .
                        "<td>" . $condition . "</td>" .
                        "<td>" . $row->price . " ,-</td>" .
                        "<td>" . $row->seller . "</td>" .
                        "<td><div id=Buy_now' class='btn btn-warning'><a href='Kup_ksiazke/numer_ksiazki/$link'>Dodaj do koszyka</a></div>" .
                        "</tr>";
            }

            return $data;
        } else if ($query->num_rows() === 1) {
            foreach ($query->result() as $row) {
                $condition = $row->condition;
                if ($condition == 0) {
                    $condition = 'Używana';
                } else {
                    $condition = 'Nowa';
                }

                return
                        "<tr>" .
                        "<td><h3>" . $row->grade . "</h3></td>" .
                        "<td><img width='50'heigth='75' src='" . $row->image_path . "'/></td>" .
                        "<td>" . $row->tittle . "</td>" .
                        "<td>" . $row->author . "</td>" .
                        "<td>" . $condition . "</td>" .
                        "<td>" . $row->price . " ,-</td>" .
                        "<td>" . $row->seller . "</td>" .
                        "</tr>";
                $data['links'] = $this->pagination->create_links();
            }
        }

        return "Nie ma takiej książki";  //zwróć html z "nie ma takiej ksiazki
    }

    public function fetch_books_for_remove($limit, $start) {
        $query = $this->session->userdata('searching');
        if ($query) {
            $searching = $this->session->userdata('searching');
            $query = $searching['author'] . $searching['tittle'] . $searching['minPrice'] . $searching['maxPrice'] . $searching['minRate'] . $searching['maxRate'] . $searching['condition'] . $searching['order'];
            $query = "SELECT * FROM book WHERE active='aktywowano' AND " . $query;
        } else {
            $query = "SELECT * FROM book WHERE active = 'aktywowano' ORDER BY price ASC";
        }
        //$query = $query . " LIMIT $limit OFFSET $start";
        $query = $this->db->query($query . " LIMIT $limit OFFSET $start");
        $data = array();
        if ($query->num_rows() > 0) {
            foreach ($query->result() as $row) {
                $condition = $row->condition;
                if ($condition == 0) {
                    $condition = 'Używana';
                } else {
                    $condition = 'Nowa';
                }

                $link = $row->id_book;
                $data[] = "<tr>" .
                        "<td><h3>" . $row->grade . "</h3></td>" .
                        "<td><img width='50'heigth='75' src='" . $row->image_path . "'/></td>" .
                        "<td><a href=" . $row->tittle . '">' . $row->tittle . "</a></td>" .
                        "<td>" . $row->author . "</td>" .
                        "<td>" . $condition . "</td>" .
                        "<td>" . $row->price . " ,-</td>" .
                        "<td>" . $row->seller . "</td>" .
                        "<td><div class='confirm_advertisment' id='$link'><div class='btn btn-danger'>Usuń</div></div>"
                        . "</tr>";
            }

            return $data;
        } else if ($query->num_rows() === 1) {
            foreach ($query->result() as $row) {
                $condition = $row->condition;
                if ($condition == 0) {
                    $condition = 'Używana';
                } else {
                    $condition = 'Nowa';
                }

                return
                        "<tr>" .
                        "<td><h3>" . $row->grade . "</h3></td>" .
                        "<td><img width='50'heigth='75' src='" . $row->image_path . "'/></td>" .
                        "<td>" . $row->tittle . "</td>" .
                        "<td>" . $row->author . "</td>" .
                        "<td>" . $condition . "</td>" .
                        "<td>" . $row->price . " ,-</td>" .
                        "<td>" . $row->seller . "</td>" .
                        "</tr>";
                $data['links'] = $this->pagination->create_links();
            }
        }

        return "Nie ma takiej książki";  //zwróć html z "nie ma takiej ksiazki
    }

    public function fetch_books_for_admin($limit, $start) {
        $this->db->where('active', 'nieaktywowano');
        $this->db->limit($limit);
        $this->db->offset($start);
        $query = $this->db->get('book');
        $data = array();
        if ($query->num_rows() > 1) {
            foreach ($query->result() as $row) {
                $condition = $row->condition;
                if ($condition == 0) {
                    $condition = 'Używana';
                } else {
                    $condition = 'Nowa';
                }

                $link = $row->id_book;
                $link2 = $link . '_delete';
                $data[] = "<tr>" .
                        "<td><h4>" . $row->id_book . "</h3></td>" .
                        "<td>" . $row->grade . "</td>" .
                        "<td><img width='50'heigth='75' src='" . $row->image_path . "'/></td>" .
                        "<td><a href=" . $row->tittle . '">' . $row->tittle . "</a></td>" .
                        "<td>" . $row->author . "</td>" .
                        "<td>" . $condition . "</td>" .
                        "<td>" . $row->description . "</td>" .
                        "<td>" . $row->price . " ,-</td>" .
                        "<td>" . $row->seller . "</td>" .
                        "<td><div class='confirm_advertisment' id='$link'><div class='btn btn-info'>Zatwierdź</div></div></br><div class='remove_advertisment' id='$link2'><div class='btn btn-danger'>&nbsp&nbsp&nbsp&nbsp  Usuń &nbsp&nbsp&nbsp&nbsp&nbsp </div></div>" .
                        "</tr>";
            }

            return $data;
        } else if ($query->num_rows() === 1) {
            foreach ($query->result() as $row) {
                $condition = $row->condition;
                if ($condition == 0) {
                    $condition = 'Używana';
                } else {
                    $condition = 'Nowa';
                }

                return
                        "<tr>" .
                        "<td><h3>" . $row->grade . "</h3></td>" .
                        "<td><img width='50'heigth='75' src='" . $row->image_path . "'/></td>" .
                        "<td>" . $row->tittle . "</td>" .
                        "<td>" . $row->author . "</td>" .
                        "<td>" . $condition . "</td>" .
                        "<td>" . $row->price . " ,-</td>" .
                        "<td>" . $row->seller . "</td>" .
                        "</tr>";
                $data['links'] = $this->pagination->create_links();
            }
        }
    }

    public function new_advertisment($data) {
        $this->db->insert('book', $data);
    }

    public function get_by_author($author) {
        $this->db->distinct();

        $this->db->where('author', $author);
        $this->db->select('tittle');
        $query = $this->db->get('book');
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                echo "<option>" . $row->tittle;
            }
        }
    }

    public function get_price($id) {


        $this->db->where('id_book', $id);
        $this->db->select('price');
        $query = $this->db->get('book');
        if ($query->num_rows() > 0) {

            foreach ($query->result() as $row) {
                return $row->price;
            }
        }
    }

    public function get_users_account_status($mail) {
        $this->db->where('mail', $mail);
        $this->db->select('account_status');
        $query = $this->db->get('user');
        foreach ($query->result() as $row) {
            return $row->account_status;
        }
    }

    public function change_advertisment_activity($id) {
        $query = "UPDATE book SET active = 'aktywowano' WHERE id_book=$id";
        $this->db->query($query);
    }

    public function remove_advertisment_activity($id) {
        $query = "UPDATE book SET active = 'usunięto' WHERE id_book=$id";
        $this->db->query($query);
    }

    public function most_favoriet_books($offset) {
        $query = "SELECT * FROM book ORDER BY grade DESC LIMIT 2 OFFSET $offset";
        $query = $this->db->query($query);

        foreach ($query->result() as $row) {
            echo
            "<div class='grid-top img-pos'>"
            . "<img  src='$row->image_path' alt='' class='img-responsive'>                      
			<div class='img-caption'>
				<p>'$row->tittle' <br>Tylko $row->price ,-</p>
				<small>Kup już dziś i otrzymaj atrakcyjne bonusy!</small>
			</div>
	</div>";
        }
    }

    public function get_book_info($link) {

        $this->db->where('id_book', $link);
        $query = $this->db->get('book');

        $data = array();

        foreach ($query->result() as $row) {
            $data['id_book'] = $row->id_book;
            $data['author'] = $row->author;
            $data['tittle'] = $row->tittle;
            $data['price'] = $row->price;
            $data['description'] = $row->description;
            $data['grade'] = $row->grade;
            $data['image_path'] = $row->image_path;
            $data['seller'] = $row->seller;
        }
        return $data;
    }

    public function get_count_of_rate_info($id_book) {
        $this->db->where('id_book', $id_book);
        $this->db->select('grade_counter');
        $query = $this->db->get('book');
        
        foreach ($query->result() as $row) {
            return $row->grade_counter;
        }
        
    }
      public function get_sum_of_rate_info($id_book) {
        $this->db->where('id_book', $id_book);
        $this->db->select('grade_sum');
        $query = $this->db->get('book');
        
        foreach ($query->result() as $row) {
            return $row->grade_sum;
        }
        
    }

    public function get_actual_book_rate($id_book) {
        $this->db->where('id_book', $id_book);
        $this->db->select('grade');
        $query = $this->db->get('book');

        foreach ($query->result() as $row) {
            return $row->grade;
        }
    }

    public function update_rate($id_book, $new_rate,$rate_sum,$rate_count) {
  
        $this->db->where('id_book',$id_book);
        $data = array('grade'=>$new_rate,'grade_counter'=>$rate_count,'grade_sum'=>$rate_sum);
        $this->db->update('book',$data);
        
        
                
        
    }

}
